
#include "AxoNN.h"
#include "AxMeasure.h"


using namespace Ax;

//TODO: not thread safe 
struct MeasureInfo {
  std::vector<float> times;
  clock_t latest;
  bool isMeasuring;
};
static std::unordered_map<std::string, MeasureInfo> measure_table;

bool Measure::inHash(std::string id) {
  // TODO: making it a pointer can optimize it significantly, and we want optimization here
  std::unordered_map<std::string, MeasureInfo>::const_iterator got = measure_table.find(id);
  if (got == measure_table.end()) {
    return false;
  } else {
    return true;
  }
}

//TODO: char* id is better
void Measure::start(std::string id) {
  if (inHash(id)) {
    MeasureInfo mi = measure_table[id];
    assert(mi.isMeasuring == false);
    mi.isMeasuring = true;
    mi.latest = clock();
    measure_table[id] = mi;
  } else {
    MeasureInfo mi;
    mi.isMeasuring = true;
    mi.latest = clock();
    measure_table[id] = mi;
  }
}

void Measure::stop(std::string id) {
  assert(inHash(id));
  MeasureInfo mi = measure_table[id];
  clock_t stop = clock();
  mi.times.push_back(float(stop - mi.latest) / CLOCKS_PER_SEC);
  mi.isMeasuring = false;
  measure_table[id] = mi;
}

//TODO: return instead of print
void Measure::printAvg(std::string id) {
  assert(inHash(id));
  MeasureInfo mi = measure_table[id];
  assert(mi.isMeasuring == false);
  int count = 0;
  float sum = 0.0;
  for (float time : mi.times) {
    sum += time;
    count++;
  }

  std::cout << "TIME <" << id << "> : " << sum / count << std::endl;
}
