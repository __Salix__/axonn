
#include "AxoNN.h"
#include "AxNeuron.h"
#include "AxSynapse.h"
#include "AxOracle.h"


using namespace std;
using namespace Ax;

static Random bias_generator;

Neuron::Neuron()
  :id(""), flag(nullptr) {}

Neuron::Neuron(string id_)
  :id(id_), flag(nullptr) {
  Oracle::add_neuron(id, this);
}


InputNeuron::InputNeuron()
  :super() {}
InputNeuron::InputNeuron(std::string id_)
  :super(id_) {}


HiddenNeuron::HiddenNeuron()
  :super() {

  bias = bias_generator.generate();
}
HiddenNeuron::HiddenNeuron(std::string id_)
  :super(id_) {

  bias = bias_generator.generate();
}


OutputNeuron::OutputNeuron()
  :super() {

  bias = bias_generator.generate();
}
OutputNeuron::OutputNeuron(std::string id_)
  :super(id_) {

  bias = bias_generator.generate();
}

string InputNeuron::debug_info() {
  stringstream ss;
  ss << "Input Neuron:\n";
  ss << "\tid             : " << id << "\n";
  ss << "\tpointer        : 0x" << this << "\n";
  ss << "\tcurrent output : " << input << "\n";
  ss << "\toutput synapses: " << output_connections.size() << "\n";
  for (Synapse* s : output_connections) {
    ss << "\tSyanapse:\n";
    ss << "\t\tid             : " << s->id << "\n";
    ss << "\t\tpointer        : 0x" << s << "\n";
    ss << "\t\tweight         : " << s->weight << "\n";
    ss << "\t\tconnected to   : " << s->to_neuron << "\n";
  }

  return ss.str();
}

string HiddenNeuron::debug_info() {
  stringstream ss;
  ss << "Hidden Neuron:\n";
  ss << "\tid             : " << id << "\n";
  ss << "\tpointer        : 0x" << this << "\n";
  ss << "\tbias           : " << bias << "\n";
  ss << "\tcurrent output : " << potential.output << "\n";
  ss << "\tinput synapses: " << input_connections.size() << "\n";
  for (Synapse* s : input_connections) {
    ss << "\tSyanapse:\n";
    ss << "\t\tid             : " << s->id << "\n";
    ss << "\t\tpointer        : 0x" << s << "\n";
    ss << "\t\tweight         : " << s->weight << "\n";
    ss << "\t\tconnected from : " << s->from_neuron << "\n";
  }
  ss << "\toutput synapses: " << output_connections.size() << "\n";
  for (Synapse* s : output_connections) {
    ss << "\tSyanapse:\n";
    ss << "\t\tid             : " << s->id << "\n";
    ss << "\t\tpointer        : 0x" << s << "\n";
    ss << "\t\tweight         : " << s->weight << "\n";
    ss << "\t\tconnected to   : " << s->to_neuron << "\n";
  }
  return ss.str();
}

string OutputNeuron::debug_info() {
  stringstream ss;
  ss << "Output Neuron:\n";
  ss << "\tid             : " << id << "\n";
  ss << "\tpointer        : 0x" << this << "\n";
  ss << "\tbias           : " << bias << "\n";
  ss << "\tcurrent output : " << potential.output << "\n";
  ss << "\tinput synapses: " << input_connections.size() << "\n";
  for (Synapse* s : input_connections) {
    ss << "\tSyanapse:\n";
    ss << "\t\tid             : " << s->id << "\n";
    ss << "\t\tpointer        : 0x" << s << "\n";
    ss << "\t\tweight         : " << s->weight << "\n";
    ss << "\t\tconnected from : " << s->from_neuron << "\n";
  }
  return ss.str();
}


