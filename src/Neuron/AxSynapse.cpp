
#include "AxoNN.h"
#include "AxSynapse.h"
#include "AxOracle.h"

using namespace std;
using namespace Ax;

Synapse::Synapse()
  :id(""), from_neuron(nullptr), to_neuron(nullptr), flags(0) {
  weight = weight_generator.generate();
}
Synapse::Synapse(std::string id_)
  :id(id_), from_neuron(nullptr), to_neuron(nullptr), flags(0) {
  weight = weight_generator.generate();

  Oracle::add_synapse(id, this);

}



