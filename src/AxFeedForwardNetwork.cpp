
#include "AxoNN.h"
#include "AxFeedForwardNetwork.h"

using namespace Ax;
using namespace std;

// Does a shallow copy of the samples
void FeedForwardNetwork::setup_learning_set(Sample* samples, size count) {
  learning_set = samples;
  learning_set_size = count;
}

void FeedForwardNetwork::setup_neural_network(const vector<InputNeuron*>& input_neurons_, const vector<OutputNeuron*>& output_neurons_) {
  // Checks
  // TODO: implement find function in Vector
  /*
  AX_DBG_IF(input_neurons_.find(nullptr) || input_neurons_.find(nullptr)) {
    Logger::Error("Uninitialized output neuron");
    return;
  }
  */

  input_neurons = input_neurons_;
  output_neurons = output_neurons_;

  nm.initialize(&input_neurons, &output_neurons);
}

void FeedForwardNetwork::finalize_neural_network() {
  nm.finalize();
}

// May reduce caching for Matrices, but easier to paralelize this
void FeedForwardNetwork::teach_neural_network_single(const Sample& sample, real eta) {
  // Forwardpropagation
  nm.setup_forward_propagation(sample.inputs); // initializes the sample input
  nm.forward_propagate(); // perform forward propagation for entire graph network
  vector<real> experimental = nm.calculate_propagation_results();

  // Backpropagation
  nm.back_propogate_outputs(experimental, sample.outputs);
  nm.back_propogate_hiddens();
  nm.update_weights(eta);
}

void FeedForwardNetwork::teach_neural_network(real eta) {

  for (size i = 0; i < learning_set_size; i++) {
    teach_neural_network_single(learning_set[i], eta);
  }


  //Measure::printAvg("setup_forward_propagation");
  //Measure::printAvg("forward_propagate");
  //Measure::printAvg("calculate_propagation_results");
  //Measure::printAvg("back_propogate_outputs");
  //Measure::printAvg("back_propogate_hiddens");
  //Measure::printAvg("update_weights");

}
vector<real> FeedForwardNetwork::test_neural_network(const Sample& sample) {
  nm.setup_forward_propagation(sample.inputs);
  nm.forward_propagate();
  vector<real> error;
  vector<real> results = nm.calculate_propagation_results();
  for (size i = 0; i < results.size(); i++) {
    error.push_back(results[i] - sample.outputs[i]);
  }
  return error;
}


