
#include "AxoNN.h"
#include "AxMatrix.h"


using namespace Ax;

Matrix::Matrix() :
width(1), height(1) {
  //values = new real[1];
  values = (real*)malloc(sizeof(real));
}
Matrix::Matrix(const size w, const size h) :
width(w), height(h) {
  //values = new real[w * h];
  values = (real*)malloc(sizeof(real) * w * h);
}
Matrix::Matrix(const Matrix& other) :
width(other.width), height(other.height) {
  //values = new real[width * height];
  values = (real*)malloc(sizeof(real)* width * height);
  for (size i = 0; i < width * height; i++) {
    values[i] = other.values[i];
  }
}

Matrix& Matrix::operator= (const Matrix& other) {
  if (this != &other) {
    //real* new_values = new real[other.width * other.height];
    real* new_values = 
      (real*)malloc(sizeof(real)* other.width * other.height);
    for (size i = 0; i < other.width * other.height; i++) {
      new_values[i] = other.values[i];
    }
    //delete[] values;
    free(values);
    values = new_values;
    width = other.width;
    height = other.height;
  }
  return *this;
}

Matrix::~Matrix() {
  //delete[] values;
  free(values);
}


__SubMatrix Matrix::operator[] (size w) const {
  AX_DBG_IF(w >= width) {
    Logger::Fatal("Width Overflow");
  }
  return __SubMatrix(&values[w * height]);
}


void Matrix::print() {
  for (size h = 0; h < height; h++) {
    for (size w = 0; w < width; w++) {
      std::cout << std::setprecision(3) << (*this)[w][h] << "  ";
    }
    std::cout << std::endl;
  }
}

size Matrix::Width() {
  return width;
}

size Matrix::Height() {
  return height;
}

Matrix Matrix::ew(real operate(real)) {
  Matrix y(this->width, this->height);
  for (size i = 0; i < width; i++) {
    for (size j = 0; j < height; j++) {
      y[i][j] = operate((*this)[i][j]);
    }
  }
  return y;
}

// Friends

namespace Ax {

  Matrix operator+ (const Matrix& a) {
    Matrix c = a;
    return c;
  }

  Matrix operator- (const Matrix& a) {
    Matrix c(a.width, a.height);
    return c;
  }


  Matrix operator+ (const Matrix& a, const Matrix& b) {
    AX_DBG_IF(a.height != b.height || a.width != b.width) {
      Logger::Fatal("Dimensions do not match");
    }
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] + b[i][j];
      }
    }
    return c;
  }

  Matrix operator- (const Matrix& a, const Matrix& b) {
    AX_DBG_IF(a.height != b.height || a.width != b.width) {
      Logger::Fatal("Dimensions do not match");
    }
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] - b[i][j];
      }
    }
    return c;
  }


  Matrix ew_mul(const Matrix& a, const Matrix& b) {
    AX_DBG_IF(a.height != b.height || a.width != b.width) {
      Logger::Fatal("Dimensions do not match");
    }
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] * b[i][j];
      }
    }
    return c;
  }

  Matrix ew_div(const Matrix& a, const Matrix& b) {
    AX_DBG_IF(a.height != b.height || a.width != b.width) {
      Logger::Fatal("Dimensions do not match");
    }
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] / b[i][j];
      }
    }
    return c;
  }

  Matrix operator* (const Matrix& a, const real scalar) {
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] * scalar;
      }
    }
    return c;
  }

  Matrix operator* (const real scalar, const Matrix& a) {
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] * scalar;
      }
    }
    return c;
  }

  Matrix operator/ (const Matrix& a, const real scalar) {
    Matrix c(a.width, a.height);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[i][j] = a[i][j] / scalar;
      }
    }
    return c;
  }

  Matrix operator* (const Matrix& a, const Matrix& b) {
    AX_DBG_IF(a.width != b.height) {
      Logger::Fatal("Dimensions do not match");
    }
    Matrix c(b.width, a.height);
    for (size row = 0; row < c.width; row++) {
      for (size col = 0; col < c.height; col++) {
        real sum = 0.0;
        for (size i = 0; i < a.width; i++) {
          sum += a[i][col] * b[row][i];
        }
        c[row][col] = sum;
      }
    }
    return c;
  }

  // transpose matrix
  Matrix trans(const Matrix a) {
    Matrix c(a.height, a.width);
    for (size i = 0; i < a.width; i++) {
      for (size j = 0; j < a.height; j++) {
        c[j][i] = a[i][j];
      }
    }
    return c;
  }

  // determinant of the matrix
  real det(const Matrix& a) {
    //TODO:
    Logger::Fatal("Implement this function pls");
    Matrix c;
    return 0.0;
  }

  // adjugate (adjoint) matrix
  Matrix adj(const Matrix a) {
    Matrix c;
    Logger::Fatal("Implement this function pls");
    return c;
  }

  Matrix inverse(const Matrix a) {
    Matrix c;
    Logger::Fatal("Implement this function pls");
    return c;
  }
  // eigenvalue solver


  // Fast Functions

}
