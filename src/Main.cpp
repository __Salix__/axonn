
//////////////////////////////
#if defined(_WIN32) || defined(_WIN64)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
/////////////////////////////

#include <iostream>
#include "AxoNN.h"

#include "AxOutputNeuron.h"
#include "AxILNetworkManager.h"
#include <time.h> 

#include "AxMeasure.h"

#include "AxMatrix.h"


using namespace std;



#define N 1000000

int main(int argc, char** argv){
#if defined(_WIN32) || defined(_WIN64)
  _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif


  int* i = (int*)malloc(sizeof(int)* 10);
  cout << "version " << AXONN_MAJ_VERSION << "." << AXONN_MIN_VERSION << "\n";

  // Xor Example

  Ax::InputNeuron i0("i0");
  Ax::InputNeuron i1("i1");

  Ax::HiddenNeuron h0("h0");
  Ax::HiddenNeuron h1("h1");

  Ax::OutputNeuron out("out");

  Ax::Synapse i0h0("i0h0"), i0h1("i0h1"), i1h0("i1h0"), i1h1("i1h1"),
    h0out("h0out"), h1out("h1out");


  Ax::ILNetworkManager::setup_connection(&i0, &i0h0, &h0);
  Ax::ILNetworkManager::setup_connection(&i0, &i0h1, &h1);
  Ax::ILNetworkManager::setup_connection(&i1, &i1h0, &h0);
  Ax::ILNetworkManager::setup_connection(&i1, &i1h1, &h1);
  Ax::ILNetworkManager::setup_connection(&h0, &h0out, &out);
  Ax::ILNetworkManager::setup_connection(&h1, &h1out, &out);


  vector<Ax::InputNeuron*> ins;
  ins.push_back(&i0);
  ins.push_back(&i1);
  vector<Ax::OutputNeuron*> outs;
  outs.push_back(&out);

  
  srand(time(NULL));
  Ax::Measure::start("new");
  Ax::Sample* learning_set = new Ax::Sample[N];
  Ax::Measure::stop("new");
  Ax::Measure::printAvg("new");

  Ax::Measure::start("learning set");
  for (int i = 0; i < N; i++) {
    int k = rand();
    int x = k % 2;
    int y = (k >> 1) % 2;
    int z = x ^ y;


    vector<Ax::real> inputs = { (Ax::real) x, (Ax::real) y};
    vector<Ax::real> outputs = { (Ax::real) z };
    learning_set[i] = Ax::Sample(inputs, outputs);
  }
  Ax::Measure::stop("learning set"); //2.65
  Ax::Measure::printAvg("learning set");

  Ax::FeedForwardNetwork ffn;
  ffn.setup_learning_set(learning_set, N);
  Ax::Matrix a(2, 2);
  a[0][0] = 0.3;
  a[1][0] = -0.5;
  a[0][1] = -0.2;
  a[1][1] = 0.7;
  Ax::Matrix b(1, 2);
  b[0][0] = 0.5;
  b[0][1] = -0.1;
  Ax::Matrix bias_a(2, 1);
  bias_a[0][0] = 0.7;
  bias_a[1][0] = -0.4;
  Ax::Matrix bias_b(1, 1);
  bias_b[0][0] = 0.9;
  vector<Ax::Matrix> weights = { a, b };
  vector<Ax::Matrix> biases = { bias_a, bias_b };
  ffn.setup_neural_network(weights, biases);
  //ffn.debug();

  Ax::Measure::start("teach_neural_network");
  ffn.teach_neural_network(0.1);
  Ax::Measure::stop("teach_neural_network");
  Ax::Measure::printAvg("teach_neural_network");

  for (int i = 0; i < 4; i++){
    int x = (i >> 0) & 1;
    int y = (i >> 1) & 1;
    int z = x ^ y;
    vector<Ax::real> inputs = { (Ax::real) x, (Ax::real) y };
    vector<Ax::real> outputs = { (Ax::real) 0.0 };
    vector<Ax::real> errors = ffn.test_neural_network(Ax::Sample(inputs, outputs));

    cout << "X : " << x << endl;
    cout << "Y : " << y << endl;
    cout << "Z : " << z << endl;
    cout << "Z': " << errors[0] << endl;
    cout << endl;

  }


  //ffn.debug();

  ffn.finalize_neural_network();
  delete[] learning_set;


  system("PAUSE");
  return 0;
}