
#include "AxoNN.h"
#include "AxILNetworkManager.h"

using namespace Ax;
using namespace std;

void ILNetworkManager::setup_connection(SourceNeuron* from, Synapse* connector, DrainNeuron* to) {
  AX_DBG_IF(from == nullptr) Logger::Fatal("variable <from> not initialized");
  AX_DBG_IF(to == nullptr) Logger::Fatal("variable <to> not initialized");
  AX_DBG_IF(connector == nullptr) Logger::Fatal("variable <connector> not initialized");
  AX_DBG_IF(connector->from_neuron != nullptr || connector->to_neuron != nullptr)
    Logger::Warning("The synapse already has a connection to a neuron");

  connector->from_neuron = from;
  connector->to_neuron = to;

  from->output_connections.push_back(connector);
  to->input_connections.push_back(connector);
}


void ILNetworkManager::initialize(vector<InputNeuron*>* input_neurons_, vector<OutputNeuron*>* output_neurons_) {

  AX_DBG_IF(input_neurons_ == nullptr || output_neurons_ == nullptr){
    Logger::Error("input or output neuron containers not defined");
  } else {
    // Set the input and output 
    input_neurons = input_neurons_;
    output_neurons = output_neurons_;

    topological_sort();
    setup_delta_flags();
  }
}

void ILNetworkManager::finalize() {
  remove_delta_flags();
}



void ILNetworkManager::setup_delta_flags() {
  // TODO: are input neurons really needed? 
  // well, no, but should we put them in anyway as to not cause confusion
  for (InputNeuron* n : *input_neurons) {
    n->flag = new real;
  }
  for (HiddenNeuron* n : hidden_neurons) {
    n->flag = new real;
  }
  for (OutputNeuron* n : *output_neurons) {
    n->flag = new real;
  }
}

void ILNetworkManager::remove_delta_flags() {

  for (InputNeuron* n : *input_neurons) {
    delete n->flag;
    n->flag = nullptr;
  }
  for (HiddenNeuron* n : hidden_neurons) {
    delete n->flag;
    n->flag = nullptr;
  }
  for (OutputNeuron* n : *output_neurons) {
    delete n->flag;
    n->flag = nullptr;
  }
}


//TODO: I really don't like this function
// alternatives: set up a struct and avoid using this function entirely
real* ILNetworkManager::delta(Neuron* n) {
  return (real*)n->flag;
}

// Helper functions unrelated to the ILNetworkManager class
// Note: only works on drain neurons. If the neuron is a source neuron
// it will not have any input connections and therefore this function is
// useless
bool ILNetworkManager::no_incoming_edge(DrainNeuron* n){
  for (Synapse* syn : n->input_connections){
    if (syn->flags == 0){
      return false;
    }
  }
  return true;
}

void ILNetworkManager::initialize_input_neuron(InputNeuron* n, real input) {
  n->input = input;
}

void ILNetworkManager::update_hidden_neuron(HiddenNeuron* n) {
  real sum = n->bias;
  // O_i = sigmoid(SUM (O_j * W_ij))
  for (Synapse* s : n->input_connections) {
    real out = s->from_neuron->get_output() * s->weight;
    sum += out;
  }
  n->potential.input = sum;
  n->potential.output =
    n->potential.transfer_function(n->potential.input);
}

// TODO: I don't like this. too similar to the above function
real ILNetworkManager::finalize_output_neuron(OutputNeuron* n) {

  real sum = n->bias;
  // O_i = sigmoid(SUM (O_j * W_ij))
  for (Synapse* s : n->input_connections) {
    real out = s->from_neuron->get_output() * s->weight;
    sum += out;
  }
  n->potential.input = sum;
  n->potential.output =
    n->potential.transfer_function(n->potential.input);

  return n->potential.output;
}

// Forward Propagation Functions
void ILNetworkManager::setup_forward_propagation(const vector<real>& sample_inputs){


  AX_DBG_IF(input_neurons->size() != sample_inputs.size()){
    Logger::Error("sample input size does not match the network input size");
    return;
  }

  // Initializes every input to the desired sample
  size i = 0;
  for (InputNeuron* neuron : *input_neurons) {
    initialize_input_neuron(neuron, sample_inputs[i++]);
  }
}

// Note: this only forward propagates the hidden neurons.
// the functions finalize and initialize are responsible for setting up the
// input and output neurons
void ILNetworkManager::forward_propagate(){
  for (HiddenNeuron* neuron : hidden_neurons){
    update_hidden_neuron(neuron);
  }
}

vector<real> ILNetworkManager::calculate_propagation_results(){
  vector<real> results;

  for (OutputNeuron* neuron : *output_neurons) {
    results.push_back(finalize_output_neuron(neuron));
  }

  return results;
}

// Back Propogation functions



void ILNetworkManager::back_propogate_outputs(const std::vector<real> experimental, const std::vector<real> expected){
  // TODO: currently only using the standard cost function (i.e.1/2 * SIGMA (o_k - t_k))

  size i = 0;
  for (OutputNeuron* n : *output_neurons) {
    *delta(n) = n->potential.tf_derivative(n->potential.input) *
      (experimental[i] - expected[i]);
    i++;
  }
}

void ILNetworkManager::back_propogate_hiddens(){
  // TODO: currently only using the standard cost function (i.e.1/2 * SIGMA (o_k - t_k))
  // We probably are going to keep this though
  for (vector<HiddenNeuron*>::reverse_iterator i = hidden_neurons.rbegin(); i != hidden_neurons.rend(); i++) {
    HiddenNeuron* n = *i;

    //delta_j = (d/dx sigmoid(x_j)) SUM(delta_k * W_ik)
    real sum = 0.0;
    for (Synapse* s : n->output_connections) {
      sum += (*delta(s->to_neuron)) * s->weight;
    }
    *delta(n) = n->potential.tf_derivative(n->potential.input) * sum;
  }

}

void ILNetworkManager::update_weights(real eta){
  for (HiddenNeuron* neuron : hidden_neurons){
    real d = *delta(neuron);
    for (Synapse* input_synapse : neuron->input_connections) {
      //delta W = -eta * delta_l * O_(l-1)
      Neuron* prev = input_synapse->from_neuron;
      input_synapse->weight -= eta * d * prev->get_output();
    }
    // delta theta = eta * delta
    neuron->bias -= eta * d;
  }
  for (OutputNeuron* neuron : *output_neurons){
    real d = *delta(neuron);
    for (Synapse* input_synapse : neuron->input_connections) {
      Neuron* prev = input_synapse->from_neuron;
      input_synapse->weight -= eta * d * prev->get_output();
    }
    neuron->bias -= eta * d;
  }
}

// sets up the hidden neurons in proper order
// I'm using wikipedias pseudo code here
// http://en.wikipedia.org/wiki/Topological_sorting#Algorithms
void ILNetworkManager::topological_sort(){
  
  enum TOPOLOGICAL_SORT_FLAGS {
    UNVISITED = 0,
    VISITED = 1,
    INPUT_NEURON = 2,
    OUTPUT_NEURON = 3 
  };


  deque<SourceNeuron*> no_dependency;
  deque<Neuron*> ordered_list;

  // Mark the output and input neurons as such
  for (InputNeuron* neuron : *input_neurons){
    neuron->flag = (void*)INPUT_NEURON;
    no_dependency.push_back(neuron);
  }
  for (OutputNeuron* neuron : *output_neurons){
    neuron->flag = (void*)OUTPUT_NEURON;
  }


  while (!no_dependency.empty()){
    // pop element from head of no_dependency
    SourceNeuron* neuron = no_dependency[0];
    no_dependency.pop_front();

    // push element to the end of ordered_list
    ordered_list.push_back(neuron);

    for (Synapse* connection : neuron->output_connections){
      if (connection->flags == UNVISITED){
        // remove edge so as to not pass it twice
        connection->flags = VISITED;
        DrainNeuron* next = connection->to_neuron;

        // if it has no incoming edges then it is safe to push into the list
        if (no_incoming_edge(next) && next->flag != (void*)OUTPUT_NEURON) {
          next->flag = (void*)VISITED;
          // If all goes well next should already be a source neuron, because it's not an output
          no_dependency.push_back(dynamic_cast<SourceNeuron*>(next));
        }
      }
    }
  }

  // TODO: check to see if all the outputs are included
  // TODO: do a check to see if a cycle exists


  // Cleanup all the flags and set everything back to how it was
  // ordered_list is the hidden neuron vector and input neuron vector
  for (Neuron* neuron : ordered_list){
    if (neuron->flag == (void*)VISITED){
      hidden_neurons.push_back(dynamic_cast<HiddenNeuron*>(neuron));
    }
    neuron->flag = nullptr; // this is not entirely necessary because we'll be 
    // assigning it to something else anyway, but what the hell
    if (SourceNeuron* sn = dynamic_cast<SourceNeuron*>(neuron)){
      for (Synapse* syn : sn->output_connections){
        syn->flags = UNVISITED;
      }
    }
  }
  for (Neuron* neuron : *output_neurons){
    neuron->flag = (void*)UNVISITED;
  }

}

