
#ifndef __AXNEURON_H__
#define __AXNEURON_H__

#include "AxFunctions.h"
#include <sstream>

#include <vector>
#include <string>

namespace Ax {


  struct ActivePotential {
  private:
    typedef real(*TF)(real);

  public:
    real input; // sum of the weighted input neurons
    real output; // output from transfer function
    real(*transfer_function)(real);
    real(*tf_derivative)(real);

    ActivePotential()
      :transfer_function(sigmoid), 
      tf_derivative(sigmoid_derivative){}
  };

  class Synapse;

  class Neuron {
  protected:
    std::string id;

  public:
    void* flag;

  public:
    Neuron();
    Neuron(std::string id_);

    virtual real get_output() = 0;
    virtual std::string debug_info() = 0;

  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };


  class DrainNeuron : virtual public Neuron {
  protected:
    std::vector<Synapse*> input_connections;

  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };


  class SourceNeuron : virtual public Neuron {
  protected:
    std::vector<Synapse*> output_connections;

  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };

};

#endif // __AXNEURON_H__

