
#ifndef __AXHIDDENNEURON_H__
#define __AXHIDDENNEURON_H__

#include "AxNeuron.h"


namespace Ax {

  class HiddenNeuron 
    : public SourceNeuron, public DrainNeuron {


  private:
    typedef Neuron super;

    real bias;

    ActivePotential potential;

  public:
    HiddenNeuron();
    HiddenNeuron(std::string id_);

    real get_output() {
      return potential.output;
    }

    std::string debug_info();


  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };

};

#endif


