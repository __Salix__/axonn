
#ifndef __AXINPUTNEURON_H__
#define __AXINPUTNEURON_H__

#include "AxDefinitions.h"
#include "AxNeuron.h"

namespace Ax {
  class InputNeuron : public SourceNeuron {
  private:
    typedef Neuron super;

    real input;

  public:
    InputNeuron();
    InputNeuron(std::string id_);

    real get_output() {
      return input;
    }

    std::string debug_info();

  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };
};

#endif //__AXINPUTNEURON_H__

