
#ifndef __AXSYNAPSE_H__
#define __AXSYNAPSE_H__

#include "AxDefinitions.h"
#include "AxRandom.h"

#include <string>
#include <iostream>


namespace Ax {

  class Neuron;

  static Random weight_generator;
  

  class Synapse {

  private:

    std::string id;

    SourceNeuron* from_neuron;
    DrainNeuron* to_neuron;

    real weight;

  public:
    i64 flags;

  public:
    Synapse();
    Synapse(std::string id_);


  public:
    friend class Neuron;
    friend class InputNeuron;
    friend class HiddenNeuron;
    friend class OutputNeuron;
    friend class ILNetworkManager;
  };

};

#endif //__AXSYNAPSE_H__

