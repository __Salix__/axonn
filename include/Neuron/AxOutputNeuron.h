
#ifndef __AXOUTPUTNEURON_H__
#define __AXOUTPUTNEURON_H__

#include "AxNeuron.h"

namespace Ax {

  class OutputNeuron : public DrainNeuron {

  private:
    typedef Neuron super;

    real bias;

    ActivePotential potential;

  public:
    OutputNeuron();
    OutputNeuron(std::string id_);

    real get_output() {
      return potential.output;
    }

    std::string debug_info();

  public:
    friend class Synapse;
    friend class ILNetworkManager;
  };
};

#endif //__AXOUTPUTNEURON_H__

