
///////////////////////////////////////////////////////////////////////////////
// AxFeedForwardNetwork.h
//
//
// @version 0.1.0 06/09/2015
// @author Atta Zadeh
//
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __AXFEEDFORWARDNETWORK_H__
#define __AXFEEDFORWARDNETWORK_H__

#include "AxNetwork.h"
#include "AxILNetworkManager.h"
#include "AxMatrixNetworkManager.h"
#include <iostream>

namespace Ax {
  class FeedForwardNetwork : public Network {
  private:
    MatrixNetworkManager nm;

  public:

    //void debug(){
    //  Logger::Verbose("Debugging network configuration: ");
    //  nm.debug();
    //}

    void setup_learning_set(Sample* samples, size count);
    void setup_neural_network(const std::vector<InputNeuron*>& input_neurons_, const std::vector<OutputNeuron*>& output_neurons_);
    void setup_neural_network(std::vector<Matrix> weights, std::vector<Matrix> biases) {
      nm.initialize(weights, biases);
    }
    void finalize_neural_network();

    void teach_neural_network(real eta = 0.01);
    void teach_neural_network_single(const Sample& sample, real eta = 0.01);
    std::vector<real> test_neural_network(const Sample& sample);
  };
};

#endif //__AXFEEDFORWARDNETWORK_H__

