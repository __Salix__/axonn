
#ifndef __AXMATRIXNETWORKMANAGER_H__
#define __AXMATRIXNETWORKMANAGER_H__

#include "AxLogger.h"

#include "AxSynapse.h"

#include "AxNeuron.h"
#include "AxInputNeuron.h"
#include "AxOutputNeuron.h"
#include "AxHiddenNeuron.h"
#include "AxMatrix.h"

#include "AxNetworkManager.h"

namespace Ax {

  class Layer {
    Matrix weights;
    std::vector<DrainNeuron*> neurons;
  };

  class InputLayer {
    std::vector<SourceNeuron*> neurons;
  };

  class MatrixNetworkManager :
    public NetworkManager {

  private:
    // http://neuralnetworksanddeeplearning.com/chap2.html

    Matrix input_matrix;
    Matrix output_matrix; //TODO: not needed
    // forward propagation
    size layers_num; //L (except the ls are a little bit different from the source)
    std::vector<Matrix> weights; // w
    std::vector<Matrix> biases; // b
    std::vector<Matrix> results; // z
    std::vector<Matrix> activations; // a
    // std::vector<Matrix> filter; // TODO: sometimes, not all weights are available (i.e. it is sparsely connected)
    // back propagation
    std::vector<Matrix> deltas;

  public:

    void initialize(std::vector<InputNeuron*>* input_neurons_, std::vector<OutputNeuron*>* output_neurons_) {}
    void finalize() {}

    //TODO: Temp (or is it??)
    void initialize(std::vector<Matrix> weight_matrices, std::vector<Matrix> bias_matrices) {
      AX_DBG_IF(weight_matrices.size() != bias_matrices.size()) {
        Logger::Error("layers do not match");
        return;
      }
      //TODO: check for proper dimensions
      weights = weight_matrices;
      biases = bias_matrices;
      layers_num = weight_matrices.size();
    }

    // Forward Propagation Functions
    void setup_forward_propagation(const std::vector<real>& sample_inputs) {
      //Measure::start("setup_forward_propagation");
      input_matrix = Matrix(sample_inputs.size(), 1);
      for (size i = 0; i < sample_inputs.size(); i++) {
        input_matrix[i][0] = sample_inputs[i];
      }
      //Measure::stop("setup_forward_propagation");
    }

    // input and output neurons
    void forward_propagate() {
      //Measure::start("forward_propagate");
      results = std::vector<Matrix>();
      activations = std::vector<Matrix>();

      results.push_back(input_matrix);
      activations.push_back(input_matrix);

      output_matrix = input_matrix;
      for (size i = 0; i < layers_num; i++) {

        results.push_back(
          activations[i] * weights[i] + biases[i]);

        activations.push_back(
          results.back().ew(sigmoid));// TODO: not necessarily sigmoid
      }
      //Measure::stop("forward_propagate");
    }
    std::vector<real> calculate_propagation_results() {
      //Measure::start("calculate_propagation_results");
      std::vector<real> results;
      output_matrix = activations.back();
      for (size i = 0; i < output_matrix.Width(); i++) {
        results.push_back(output_matrix[i][0]);
      }
      //Measure::stop("calculate_propagation_results");
      return results;
    }

    Matrix rowMatrix(std::vector<real> vec) {
      Matrix m(vec.size(), 1);
      for (size i = 0; i < vec.size(); i++) {
        m[i][0] = vec[i];
      }
      return m;
    }

    // Back Propogation Functions
    void back_propogate_outputs(const std::vector<real> experimental, const std::vector<real> expected) {
      //Measure::start("back_propogate_outputs");
      //delta^L = (a^L - y) ew* sigmoid'(z^L)
      Matrix exprim_matrix = rowMatrix(experimental);
      Matrix expect_matrix = rowMatrix(expected);

      deltas = std::vector<Matrix>(layers_num);
      deltas[layers_num-1] = ew_mul(
        exprim_matrix - expect_matrix,
        results[layers_num].ew(sigmoid_derivative));

      //Measure::stop("back_propogate_outputs");
    }
    void back_propogate_hiddens() {
      //Measure::start("back_propogate_hiddens");
      //delta^l = ((w^l+1)^T delta^l+1) ew* sigmoid'(z^l)
      for (size i = layers_num - 1; i > 0; i--) {
        deltas[i-1] = ew_mul(
          deltas[i] * trans(weights[i]), //TODO: did I swith them?
          results[i].ew(sigmoid_derivative));
      }
      //Measure::stop("back_propogate_hiddens");
    }
    void update_weights(real eta) {
      //Measure::start("update_weights");
      for (size i = 0; i < layers_num; i++) {
        // Dw = delta * result_matrices
        weights[i] = weights[i] - eta * (trans(activations[i]) * deltas[i]); //TODO: switch them again?

        // Db = delta
        biases[i] = biases[i] - eta * deltas[i];
      }
      //Measure::stop("update_weights");
    }


  };

};



#endif

