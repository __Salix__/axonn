
#ifndef __AXNETWORKMANAGER_H__
#define __AXNETWORKMANAGER_H__

#include "AxLogger.h"

#include "AxSynapse.h"

#include "AxNeuron.h"
#include "AxInputNeuron.h"
#include "AxOutputNeuron.h"
#include "AxHiddenNeuron.h"

namespace Ax {

  class NetworkManager {

  public:

    virtual void initialize(std::vector<InputNeuron*>* input_neurons_, std::vector<OutputNeuron*>* output_neurons_) = 0;
    virtual void finalize() = 0;

    // Forward Propagation Functions
    virtual void setup_forward_propagation(const std::vector<real>& sample_inputs) = 0;

    // input and output neurons
    virtual void forward_propagate() = 0;
    virtual std::vector<real> calculate_propagation_results() = 0;

    // Back Propogation Functions
    virtual void back_propogate_outputs(const std::vector<real> experimental, const std::vector<real> expected) = 0;
    virtual void back_propogate_hiddens() = 0;
    virtual void update_weights(real eta) = 0;
  };
};

#endif

