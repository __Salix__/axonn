
#ifndef __AXILNETWORKMANAGER_H__
#define __AXILNETWORKMANAGER_H__

#include "AxLogger.h"

#include "AxSynapse.h"

#include "AxNeuron.h"
#include "AxInputNeuron.h"
#include "AxOutputNeuron.h"
#include "AxHiddenNeuron.h"

#include "AxNetworkManager.h"

#include <deque>

namespace Ax {

  class ILNetworkManager 
    : public NetworkManager {

  private:
    std::vector<InputNeuron*>* input_neurons;
    std::vector<HiddenNeuron*> hidden_neurons;
    std::vector<OutputNeuron*>* output_neurons;

  public:

    static void setup_connection(SourceNeuron* from, Synapse* connector, DrainNeuron* to);

  private:

    bool no_incoming_edge(DrainNeuron* n);

    void initialize_input_neuron(InputNeuron* neuron, real input);
    void update_hidden_neuron(HiddenNeuron* neuron);
    real finalize_output_neuron(OutputNeuron* neuron);
    // sets up the hidden neurons in proper order
    void topological_sort();


    real* delta(Neuron* n);

    void setup_delta_flags();
    void remove_delta_flags();

  public:
    ILNetworkManager() : input_neurons(nullptr), output_neurons(nullptr) {}
    void initialize(std::vector<InputNeuron*>* input_neurons_, std::vector<OutputNeuron*>* output_neurons_);
    void finalize();

    void debug(){
      Logger::Verbose("INPUT NEURONS:");
      for (InputNeuron* n : *input_neurons) {
        Logger::Verbose(n->debug_info());
      }
      Logger::Verbose("HIDDEN NEURONS:");
      for (HiddenNeuron* n : hidden_neurons) {
        Logger::Verbose(n->debug_info());
      }
      Logger::Verbose("OUTPUT NEURONS:");
      for (OutputNeuron* n : *output_neurons) {
        Logger::Verbose(n->debug_info());
      }
    }
    
    
    // Forward Propagation Functions
    void setup_forward_propagation(const std::vector<real>& sample_inputs);

    // Note: this only forward propagates the hidden neurons.
    // the functions finalize and initialize are responsible for setting up the
    // input and output neurons
    void forward_propagate();
    std::vector<real> calculate_propagation_results();

    // Back Propogation Functions
    void back_propogate_outputs(const std::vector<real> experimental, const std::vector<real> expected);
    void back_propogate_hiddens();
    void update_weights(real eta);


  };

}

#endif

