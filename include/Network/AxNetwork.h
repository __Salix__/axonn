
#ifndef __AXNETWORK_H__
#define __AXNETWORK_H__

#include "AxNeuron.h"
#include "AxInputNeuron.h"
#include "AxOutputNeuron.h"
#include "AxSample.h"

#include <vector>

namespace Ax {

  class Network {
  protected:
    // Using a pointer to learning_set to improve performance
    Sample* learning_set;
    size learning_set_size;

    //TODO: not being used at the moment
    Sample* verification_set;
    size verification_set_size;


    std::vector<InputNeuron*> input_neurons;
    std::vector<OutputNeuron*> output_neurons;
  public:

    Network()
      :learning_set(nullptr), learning_set_size(0),
      verification_set(nullptr), verification_set_size(0) {}

  };

};

#endif //__AXNETWORK_H__

