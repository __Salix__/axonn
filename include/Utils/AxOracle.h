
#ifndef __AXORACLE_H__
#define __AXORACLE_H__

#include <unordered_map>

namespace Ax {

  class Neuron;
  class Synapse;

  class Oracle {
  private:
    static std::unordered_map<std::string, Neuron*> neuron_map;
    static std::unordered_map<std::string, Synapse*> synapse_map;

  public:
    static void add_neuron(std::string id, Neuron* neuron);
    static void add_synapse(std::string id, Synapse* synapse);
  };

};

#endif

