
#ifndef __AXEXCEPTIONS_H__
#define __AXEXCEPTIONS_H__


#include "AxoNN.h"
#include <exception>
// #include <stdexcept> is unnecessary


namespace Ax {

  class ConfigFileError : public std::exception {};

};

#endif /* __AXEXCEPTIONS_H__ */

