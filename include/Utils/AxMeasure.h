
#ifndef __AXMEASURE_H__
#define __AXMEASURE_H__

#include <unordered_map>
#include <string>
#include <ctime>
#include <vector>
#include <iostream>
#include <assert.h> //TODO: replace assert




namespace Ax {

  class Measure {

  private:
    static bool inHash(std::string id);
  public:
    //TODO: char* id is better
    static void start(std::string id);
    static void stop(std::string id);
    //TODO: return instead of print
    static void printAvg(std::string id);
  };
};

#endif

