
#ifndef __AXSAMPLE_H__
#define __AXSAMPLE_H__

#include "AxDefinitions.h"

#include <vector>

namespace Ax {

  class Sample {
  public:
    std::vector<real> inputs;
    std::vector<real> outputs;

  public:
    Sample(){}
    Sample(const std::vector<real>& inputs_, const std::vector<real>& outputs_){
      inputs = inputs_;
      outputs = outputs_;
    }

    real input(size at){
      AX_DBG_IF(at >= inputs.size() || at < 0){
        return 0.0;
      } else {
        return inputs[at];
      }
    }

    real output(size at){
      AX_DBG_IF(at >= outputs.size() || at < 0){
        return 0.0;
      } else {
        return outputs[at];
      }
    }


    size input_size(){
      return inputs.size();
    }

    size output_size(){
      return outputs.size();
    }

    

  };

};

#endif //__AXSAMPLE_H__

