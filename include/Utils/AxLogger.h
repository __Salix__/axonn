
#ifndef __AXLOGGER_H__
#define __AXLOGGER_H__

#include <iostream>
#include <string>

#if (AXONN_DEBUG == 1)
#define AX_DBG_IF(x) \
if (x)
#else //(AXONN_DEBUG == 1)
#define AX_DBG_IF(x) \
if (0)
#endif //(AXONN_DEBUG == 1)

#define AX_DBG_LINECONTROL \
__FILE__,__LINE,__func__

namespace Ax {
  class Logger {

  public:

    static void Fatal(std::string message){
      #if (AXONN_DEBUG == 1)
      std::cout << "FATAL ERROR: " << message << std::endl;
      exit(1);
      #endif // (AXONN_DEBUG == 1)
    }

    static void Error(std::string message){
      #if (AXONN_DEBUG == 1)
      std::cout << "ERROR: " << message << std::endl;
      #endif // (AXONN_DEBUG == 1)
    }


    static void Warning(std::string message){
      #if (AXONN_DEBUG == 1)
      std::cout << "WARNING: " << message << std::endl;
      #endif // (AXONN_DEBUG == 1)
    }

    static void Verbose(std::string message) {
      #if (AXONN_DEBUG == 1)
      std::cout << message << std::endl;
      #endif // (AXONN_DEBUG == 1)
    }
  };

};

#endif


