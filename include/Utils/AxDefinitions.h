
#ifndef __AXDEFINITIONS_H__
#define __AXDEFINITIONS_H__


#include "AxoNN.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string>

#include <iostream>

namespace Ax {

  typedef    size_t size;
  typedef ptrdiff_t diff;

  typedef    int8_t   i8;
  typedef   uint8_t   u8;

  typedef   int16_t  i16;
  typedef  uint16_t  u16;

  typedef   int32_t  i32;
  typedef  uint32_t  u32;

  typedef   int64_t  i64;
  typedef  uint64_t  u64;

  typedef     float  f32;
  typedef    double  f64;

#if (AXONN_FLOAT_ACCURACY == 32)
  typedef     float real;
#elif (AXONN_FLOAT_ACCURACY == 64)
  typedef    double real;
#else
  #error "Only 32 bit and 64 bit float accuracy supported"
#endif

  typedef      bool  bln;

};

#endif /* __AXDEFINITIONS_H__*/

