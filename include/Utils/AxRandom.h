
#ifndef __AXRANDOM_H__
#define __AXRANDOM_H__

#include "AxDefinitions.h"
#include <random>

namespace Ax {

  static std::random_device rd;
  static std::default_random_engine generator(rd()); // TODO: race conditions maybe?
  // TODO: make generator static 

  class Random {
  private:
    
    
    std::normal_distribution<real>* gaussian; //TODO: more distributions


  public:

    Random(){
      gaussian = new std::normal_distribution<real>(0.0, 1.0);
      //gaussian = new std::normal_distribution<real>;
    }
    Random(const Random& other){
      gaussian = new std::normal_distribution<real>(other.gaussian->param());
    }
    Random& operator= (const Random& other){
      if (this != &other){
        std::normal_distribution<real>* new_gaussian =
          new std::normal_distribution<real>(other.gaussian->param());

        delete gaussian;
        gaussian = new_gaussian;
      }
      return *this;
    }
    ~Random(){
      delete gaussian;
    }

    //void default_distribution(real mean, real stddev);
    real generate(){
      return (*gaussian)(generator);
    }

  };

};


#endif //__AXRANDOM_H__

