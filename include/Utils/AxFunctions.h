
#ifndef __AXTRANSFERFUNCTIONS_H__
#define __AXTRANSFERFUNCTIONS_H__


#include "AxDefinitions.h"

#include <cmath>

namespace Ax {

  inline real sigmoid(real x){
    return 1.0 / (1.0 + exp(-x));
  }
  inline real sigmoid_derivative(real x){
    return sigmoid(x) * (1.0 - sigmoid(x));
  }

  inline real cost_function(std::vector<real> expected, std::vector<real> experimental){
    size iterations = expected.size();
    AX_DBG_IF(expected.size() != expected.size()){
      Logger::Error("The sizes of vectors do not match");
      if (experimental.size() < expected.size()){
        iterations = experimental.size();
      }
    }

    real sum = 0.0;
    for (size i = 0; i < iterations; i++){
      sum += ((expected[0] - experimental[0]) * (expected[0] - experimental[0]));
    }
    // TODO: maybe divide by number of elements to normalize it
    return sum / 2.0;

  }

};

#endif

