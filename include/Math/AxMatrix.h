
#ifndef __AXMATRIX_H__
#define __AXMATRIX_H__

#include "AxoNN.h"
#include "AxDefinitions.h"
#include "AxLogger.h"
#include <iostream>
#include <iomanip> 

namespace Ax {

  class __SubMatrix {
  private:
    real* values;
  public:
    __SubMatrix(real* values) {
      this->values = values;
    }
    real& operator[] (size h) {
      return values[h];
    }
  };

  class Matrix {

  private:
    real* values;
    size width;
    size height;

  public:
    Matrix();
    Matrix(const size w, const size h);
    Matrix(const Matrix& other);
    Matrix& operator= (const Matrix& other);
    ~Matrix();

    //C++11 move
    Matrix(Matrix&& other)
      : width(other.width), height(other.height), values(other.values) {
      other.values = NULL;
    }
    Matrix& operator= (Matrix&& other) {
      if (this != &other) {
        free(values);
        width = other.width;
        height = other.height;
        values = other.values;
        other.values = NULL;
      }
      return *this;
    }

    __SubMatrix operator[] (size w) const;
    void print();
    size Width();
    size Height();

    Matrix ew(real operate(real));

    friend Matrix operator+ (const Matrix& a);
    friend Matrix operator- (const Matrix& a);
    friend Matrix operator+ (const Matrix& a, const Matrix& b);
    friend Matrix operator- (const Matrix& a, const Matrix& b);

    friend Matrix ew_mul(const Matrix& a, const Matrix& b);
    friend Matrix ew_div (const Matrix& a, const Matrix& b);
    friend Matrix operator* (const Matrix& a, const real scalar);

    friend Matrix operator* (const real scalar, const Matrix& a);
    friend Matrix operator/ (const Matrix& a, const real scalar);
    friend Matrix operator* (const Matrix& a, const Matrix& b);
    // transpose matrix
    friend Matrix trans(const Matrix a);
    // determinant of the matrix
    friend real det(const Matrix& a);
    // adjugate (adjoint) matrix
    friend Matrix adj(const Matrix a);
    friend Matrix inverse(const Matrix a);
    // eigenvalue solver

    // fast functions

  };

  class XMatrix {

  };

};

#endif

