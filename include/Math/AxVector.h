
#ifndef __AXVECTOR_H__
#define __AXVECTOR_H__

#include "AxoNN.h"
#include "AxDefinitions.h"
#include "AxLogger.h"


namespace Ax {

  //TODO: I really hope this doesn't ge with std::vector
  class Vector {
  public:
    friend class Matrix;

  private:
    real* values;
    size dim;

  public:
    Vector() {
      values = new real[1];
      dim = 1;
    }
    Vector(size dim_) {
      values = new real[dim_];
      dim = dim_;
    }
    Vector(real* list, size dim_) {
      values = new real[dim_];
      for (size i = 0; i < dim_; i++) {
        values[i] = list[i];
      }
      dim = dim_;
    }

    Vector(const Vector& other) {
      values = new real[other.dim];
      for (size i = 0; i < other.dim; i++) {
        values[i] = other.values[i];
      }
      dim = other.dim;
    }

    Vector& operator= (const Vector& other) {
      if (this != &other) {
        real* new_values = new real[other.dim];
        for (size i = 0; i < other.dim; i++) {
          new_values[i] = other.values[i];
        }
        delete[] values;
        values = new_values;
        dim = other.dim;
      }
      return *this;
    }
    ~Vector() {
      delete[] values;
    }
    real& operator[] (const size idx) const{
      AX_DBG_IF(idx >= dim) {
        Logger::Fatal("Height Overflow");
      }
      return values[idx];
    }
    
    real& at(const size idx) const {
      AX_DBG_IF(idx >= dim) {
        Logger::Fatal("Height Overflow");
      }
      return values[idx];
    }
    size dimensions() {
      return dim;
    }

    void print() {
      for (size i = 0; i < dim-1; i++) {
        std::cout << values[i] << ", ";
      }
      std::cout << values[dim - 1] << std::endl;
    }

    friend Vector operator+(const Vector& a) {
      Vector c = a;
      return c;
    }
    
    
    friend Vector operator-(const Vector& a) {
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = -a[i];
      }
      return c;
    }
    friend Vector operator+(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim >= b.dim) {
        Logger::Fatal("Dimensions do not match");
      }
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = a[i] + b[i];
      }
      return c;
    }
    
    friend Vector operator-(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim >= b.dim) {
        Logger::Fatal("Dimensions do not match");
      }
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = a[i] - b[i];
      }
      return c;
    }

    friend Vector ew_mul(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim >= b.dim) {
        Logger::Fatal("Dimensions do not match");
      }
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = a[i] * b[i];
      }
      return c;
    }
    friend Vector ew_div(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim >= b.dim) {
        Logger::Fatal("Dimensions do not match");
      }
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = a[i] / b[i];
      }
      return c;
    }

    friend Vector operator*(const Vector& a, const real scalar) {
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = scalar * a[i];
      }
      return c;
    }
    friend Vector operator*(const real scalar, const Vector& a) {
      return a * scalar;
    }
    friend Vector operator/(const Vector& a, const real scalar) {
      Vector c(a.dim);
      for (size i = 0; i < c.dim; i++) {
        c[i] = a[i] / scalar;
      }
      return c;
    }

    friend real dot(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim >= b.dim) {
        Logger::Fatal("Dimensions do not match");
      }
      Vector c(a.dim);
      real sum = 0.0;
      for (size i = 0; i < c.dim; i++) {
        sum += a[i] * b[i];
      }
      return sum;
    }
    friend Vector cross(const Vector& a, const Vector& b) {
      AX_DBG_IF(a.dim != 3 || b.dim != 3) {
        Logger::Fatal("Cross product vectors must be three dimensional");
      }
      Vector c(3);
      c[0] = a[1] * b[2] - a[2] * b[1];
      c[1] = a[2] * b[0] - a[0] * b[2];
      c[2] = a[0] * b[1] - a[1] * b[0];
      return c;
    }
    friend real mag(const Vector& a) {
      real sum = 0.0;
      for (size i = 0; i < a.dim; i++) {
        sum += a[i] * a[i];
      }
      return sqrt(sum);
    }
    friend real mag2(const Vector& a) {
      real sum = 0.0;
      for (size i = 0; i < a.dim; i++) {
        sum += a[i] * a[i];
      }
      return sum;
    }
    friend Vector unit(const Vector& a) {
      if (mag2(a) == 0.0) {
        return a;
      }
      return a / mag(a);
    }

  };

  class XVector {

  private:
    real* reals;
    real* imaginaries;
    size dimensions;

  };

};

#endif


