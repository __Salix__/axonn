
#ifndef __AXONN_H__
#define __AXONN_H__


#define AXONN_MAJ_VERSION 0
#define AXONN_MIN_VERSION 2
#define AXONN_REV_VERSION 0

#define AXONN_DEBUG 0

#define AXONN_FLOAT_ACCURACY 64


#include "AxDefinitions.h"
#include "AxExceptions.h"
#include "AxLogger.h"

#include "AxRandom.h"
#include "AxSample.h"

#include "AxNeuron.h"
#include "AxInputNeuron.h"
#include "AxOutputNeuron.h"
#include "AxSynapse.h"

#include "AxNetwork.h"
#include "AxFeedForwardNetwork.h"


#endif //__AXONN_H__

