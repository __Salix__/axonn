# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Version ###

Issues:
* fix Synapse classes


RoadMap:
* version 0.2 :
  * fix issue with the cost getting worse sometimes
  * Matrices for feedforwarding networks
  * Recurrent Networks

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

|                   |Classification  |Regression      |Clustering      |Rule Extraction |
|:------------------|:--------------:|:--------------:|:--------------:|:--------------:|
|**Supervised**     |               a|               b|               c|               d|
|**Unsupervised**   |               b|               c|               d|               e|
|**Semi-Supervised**|               c|               d|               e|               f|
|**Reinforcement**  |               d|               e|               f|               g|
  
### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact